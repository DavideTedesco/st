declare filename "multipink_controllable.dsp";
declare name "multipink_controllable";
declare name "multipink controllable";
declare vendor "Giuseppe Silvi & Davide Tedesco";
import("ST.lib");

volume = (hslider("volume",0,0,1,0.001)) : si.smoo;

process = multipink  : _*(checkbox("1 - LFU"))*volume,_*(checkbox("2 - RFD"))*volume,_*(checkbox("3 - RBU"))*volume,_*(checkbox("4 - LBD"))*volume;